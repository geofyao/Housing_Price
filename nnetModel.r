
#libraries
library(nnet)

#load the data
data<-read.csv("datasets/train.csv")
data_test <- read.csv("datasets/test.csv")

##get the variables that are factors to remove them from the training
factor_variables<-which(sapply(data[1,],class)=="factor")
data_preprocessed<-data[,-factor_variables]
factor_variables_test<-which(sapply(data_test[1,],class)=="factor")
data_test_preprocessed<-data_test[,-factor_variables_test]

dim(data_test_preprocessed)
dim(data_test)

#replace missing values
replace_na_with_mean_value<-function(vec) {
    mean_vec<-mean(vec,na.rm=T)
    vec[is.na(vec)]<-mean_vec
    vec
}
data_preprocessed<-data.frame(apply(data_preprocessed,2,replace_na_with_mean_value))
data_test_preprocessed<-data.frame(apply(data_test_preprocessed,2,replace_na_with_mean_value))

set.seed(3)

X<-data_preprocessed[,setdiff(colnames(data_preprocessed),"SalePrice")]
Y<-data_preprocessed[,"SalePrice"]

N<-nrow(X)    #Number of examples
n<-ncol(X)    #Number of input variables

#keep the variables that gave good results after the features selection
variables_to_keep <- c(5,17,14,26,29,18,8,28,4,2,36,11,10,27,35,12,25,7,33,21,13)

X_wrapped <- X[,variables_to_keep]
X_test <- data_test_preprocessed[,colnames(X_wrapped)]

#scale the sets

#trainging set
X_wrapped.scale <- scale(X_wrapped)
Y.scale <- scale(Y)

#test set
 meanX_wrapped <- apply(X_wrapped,2,mean) #getting the mean to scale the training set
 sdevX_wrapped <- apply(X_wrapped,2,sd) #getting the sd to scale the training set

# mean_Y <- apply(Y.scale,2,mean) #getting the mean to scale the training set
# sdev-Y <- apply(Y.scale,2,sd) #getting the sd to scale the training set

 X.ts <- scale(X_test,center =meanX_wrapped, scale = sdevX_wrapped)

#Use severals models
size.CV<-floor(nrow(X.ts)/10)
R<-20

CV.err<-numeric(10)

for (i in 1:10) {
  i.ts<-(((i-1)*size.CV+1):(i*size.CV))  
#  X.ts<-X_wrapped.scale[i.ts,]  
#  Y.ts<-Y.scale[i.ts]       
  
  i.tr<-setdiff(1:N,i.ts)                
  
  Y.hat.ts.R<-matrix(0,nrow=nrow(X.ts),ncol=R)
  
  for (r in 1:R) {
    i.tr.resample<-sample(i.tr,replace = T)   ### Complete code: Resample training set with replacement
    X.tr<-X_wrapped.scale[i.tr.resample,]
    Y.tr<-Y.scale[i.tr.resample]  
    
    DS<-cbind(X.tr,SalePrice=Y.tr)        
    model<- nnet(SalePrice~.,DS,size = 5,linout=T)
    
    Y.hat.ts.R[,r]<- predict(model,X.ts)
    
  }
  
  Y.hat.ts<-apply(Y.hat.ts.R,1,mean)
  #CV.err[i]<-sqrt(mean((Y.hat.ts-Y.ts)^2))
}

#print(paste("CV error=",round(mean(CV.err),digits=4), " ; std dev=",round(sd(CV.err),digits=4)))



Y.hat.ts <- array(t(apply(Y.scale, 1, function(r)r*attr(Y.scale,'scaled:scale') + attr(Y.scale, 'scaled:center'))),dim= c(nrow(X_test),1))

Y.hat.ts.final <- cbind(data_test$Id,Y.hat.ts)
colnames(Y.hat.ts.final)<- c("Id","SalePrice")

write.csv(Y.hat.ts.final, file = "sub.csv",row.names = FALSE)


