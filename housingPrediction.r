
#libraries
library(nnet)
library(rpart)
library(e1071)



data<-read.csv("House Prices/datasets/train.csv")
data_test <- read.csv("House Pricesdatasets/test.csv")

#dim(data)

#summary(data)

#summary(data_test)

#allow to see the class of the variable! at lot of variable are factors!
#sapply(data[1,],class)

##get the variables that are factors to remove them from the training
factor_variables<-which(sapply(data[1,],class)=="factor")
#factor_variables
data_preprocessed<-data[,-factor_variables]

replace_na_with_mean_value<-function(vec) {
    mean_vec<-mean(vec,na.rm=T)
    vec[is.na(vec)]<-mean_vec
    vec
}

data_preprocessed<-data.frame(apply(data_preprocessed,2,replace_na_with_mean_value))

#summary(data_preprocessed)

set.seed(225)

X<-data_preprocessed[,setdiff(colnames(data_preprocessed),"SalePrice")]
Y<-data_preprocessed[,"SalePrice"]

N<-nrow(X)    #Number of examples
n<-ncol(X)    #Number of input variables
dim(X)
########################
               
