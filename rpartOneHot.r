
#librairies
library(dummies)
library(corrplot)
#library(Hmisc)
library(e1071)
library(rpart)

#function
# ++++++++++++++++++++++++++++
# flattenCorrMatrix
# ++++++++++++++++++++++++++++
# cormat : matrix of the correlation coefficients
# pmat : matrix of the correlation p-values
flattenCorrMatrix <- function(cormat, pmat) {
  ut <- upper.tri(cormat)
  data.frame(
    row = rownames(cormat)[row(cormat)[ut]],
    column = rownames(cormat)[col(cormat)[ut]],
    cor  =(cormat)[ut],
    p = pmat[ut]
    )
}

#init

#load the data
data<-read.csv("House Prices/datasets/train.csv")
data_test <- read.csv("House Prices/datasets/test.csv")

##get the variables that are factors to remove them from the training
factor_variables<-which(sapply(data[1,],class)=="factor")
data_preprocessed<-data[,-factor_variables]
factor_variables_test<-which(sapply(data_test[1,],class)=="factor")
data_test_preprocessed<-data_test[,-factor_variables_test]

#replace missing values
replace_na_with_mean_value<-function(vec) {
    mean_vec<-mean(vec,na.rm=T)
    vec[is.na(vec)]<-mean_vec
    vec
}
data_preprocessed<-data.frame(apply(data_preprocessed,2,replace_na_with_mean_value))
data_test_preprocessed<-data.frame(apply(data_test_preprocessed,2,replace_na_with_mean_value))

computeRMSLE <-function(Y.hat,Y){
#Y.hat.ts[Y.hat.ts<0] <- 0    
Y.hat.log <- log(Y.hat+1) 
Y.log <- log(Y+1)
avg <- mean(Y.hat.log- Y.log)^2      
sqrt(avg)

}

set.seed(3)

X<-data_preprocessed[,setdiff(colnames(data_preprocessed),"SalePrice")]
Y<-data_preprocessed[,"SalePrice"]

N<-nrow(X)    #Number of examples
n<-ncol(X)    #Number of input variables

#test one hot encoding
data_factor<-data[,factor_variables]
data_factor_onehot <- dummy.data.frame(data_factor, sep="_")
#is.na(data_factor_onehot)

#wrapper rpart one hot
size.CV<-floor(N/10)

selected<-NULL

for (round in 1:n) { 
    candidates<-setdiff(1:n,selected)
    
    CV.err<-matrix(0,nrow=length(candidates),ncol=10)
    
    for (j in 1:length(candidates)) {
        features_to_include<-c(selected,candidates[j])
        
        for (i in 1:10) {
            i.ts<-(((i-1)*size.CV+1):(i*size.CV))  
            X.ts<-data_factor_onehot[i.ts,features_to_include,drop=F]  
            Y.ts<-Y[i.ts]
     
            i.tr<-setdiff(1:N,i.ts)
            i.tr.resample<-sample(i.tr,rep=T)
            X.tr<-data_factor_onehot[i.tr.resample,features_to_include,drop=F]
            Y.tr<-Y[i.tr.resample]
     
            DS<-cbind(X.tr,SalePrice=log(Y.tr))
            model<- svm(SalePrice~.,DS)
        
            Y.hat.ts<- predict(model,X.ts)
        
            CV.err[j,i]<-mean((Y.hat.ts-log(Y.tr))^2)
        }
    }
    CV.err.mean<-apply(CV.err,1,mean)
    CV.err.sd<-apply(CV.err,1,sd)
    selected_current<-which.min(CV.err.mean)              
    selected<-c(selected,candidates[selected_current])
    print(paste("Round ",round," ; Selected feature: ",candidates[selected_current]," ; CV error=",round(CV.err.mean[selected_current],digits=4), " ; std dev=",round(CV.err.sd[selected_current],digits=4)))

}
    


