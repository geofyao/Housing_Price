
#librairies
library(dummies)
library(corrplot)
#library(Hmisc)
#library(e1071)
#library(rpart)


remove_low_var = function(data, tol) {
  lr="\n"
  n = nrow(data)
  to.remove = NULL
  for(var in colnames(data)) {
    table = table(data[,var], exclude=NULL)
    
    variance = (max(table)-min(table))/n
    
    len =length(table)
    if(len == 1 || variance> tol) {
      to.remove = cbind(to.remove,var)
    }
  }
  cat("Removed: ", to.remove, lr)
  to.keep = setdiff(colnames(data),to.remove)
  data[,to.keep]
}



#function
# ++++++++++++++++++++++++++++
# flattenCorrMatrix
# ++++++++++++++++++++++++++++
# cormat : matrix of the correlation coefficients
# pmat : matrix of the correlation p-values
flattenCorrMatrix <- function(cormat, pmat) {
  ut <- upper.tri(cormat)
  data.frame(
    row = rownames(cormat)[row(cormat)[ut]],
    column = rownames(cormat)[col(cormat)[ut]],
    cor  =(cormat)[ut],
    p = pmat[ut]
    )
}

#init

#load the data
data<-read.csv("House Prices/datasets/train.csv")
data_test <- read.csv("House Prices/datasets/test.csv")
##get the variables that are factors to remove them from the training
factor_variables<-which(sapply(data[1,],class)=="factor")
data_preprocessed<-data[,-factor_variables]
factor_variables_test<-which(sapply(data_test[1,],class)=="factor")
data_test_preprocessed<-data_test[,-factor_variables_test]

#replace missing values
replace_na_with_mean_value<-function(vec) {
    mean_vec<-mean(vec,na.rm=T)
    vec[is.na(vec)]<-mean_vec
    vec
}
data_preprocessed<-data.frame(apply(data_preprocessed,2,replace_na_with_mean_value))
data_test_preprocessed<-data.frame(apply(data_test_preprocessed,2,replace_na_with_mean_value))

computeRMSLE <-function(Y.hat,Y){
#Y.hat.ts[Y.hat.ts<0] <- 0    
Y.hat.log <- log(Y.hat+1) 
Y.log <- log(Y+1)
avg <- mean(Y.hat.log- Y.log)^2      
sqrt(avg)

}

set.seed(3)

X<-data_preprocessed[,setdiff(colnames(data_preprocessed),"SalePrice")]
Y<-data_preprocessed[,"SalePrice"]
variables_to_keep_svm <-c(5,17,6,27,18,2,25,4,33,8,29,37,20) # c(5,17,13,8,12,6,7,4,25,15,14,28,10,26)   #svm  

X_wrapped <- X[,variables_to_keep]



#test one hot encoding
data_factor<-data[,factor_variables]
data_factor_onehot <- dummy.data.frame(data_factor)

#remove the low variance features
#var_one_hot <- as.data.frame(apply(data_factor_onehot,2,var))
#high_var_one_hot<- as.data.frame(which(var_one_hot<0.01,arr.ind=T))
#data_factor_onehot <- data_factor_onehot[,row.names.data.frame(high_var_one_hot)]
data_factor_onehot <- remove_low_var(data_factor_onehot,0.99)


X_extended<-cbind(X,data_factor_onehot)
N<-nrow(X)    #Number of examples
n<-ncol(X)    #Number of input variables
n_extended<-ncol(X_extended)    #Number of input variables
X_extended<-data.frame(apply(X_extended,2,replace_na_with_mean_value))

#wrapper rpart one hot
size.CV<-floor(N/10)

selected<-NULL

for (round in 1:(n_extended)) { 
    candidates<-setdiff(1:n_extended,selected)
    
    CV.err<-matrix(0,nrow=length(candidates),ncol=10)
    
    for (j in 1:length(candidates)) {
        features_to_include<-c(selected,candidates[j])
        
        for (i in 1:10) {
            i.ts<-(((i-1)*size.CV+1):(i*size.CV))  
            #print(features_to_include)
            X.ts<-X_extended[i.ts,features_to_include,drop=F]  
            Y.ts<-Y[i.ts]
     
            i.tr<-setdiff(1:N,i.ts)
           # i.tr.resample<-sample(i.tr,rep=T)
            X.tr<-X_extended[i.tr,features_to_include,drop=F]
            Y.tr<-Y[i.tr]
     
            DS<-cbind(X.tr,SalePrice=log(Y.tr+1))
            model<- lm(SalePrice~.,DS)
        
            Y.hat.ts<- predict(model,X.ts)
        
            CV.err[j,i]<-sqrt(mean((Y.hat.ts-log(Y.ts+1))^2))
        }
    }
    CV.err.mean<-apply(CV.err,1,mean)
    CV.err.sd<-apply(CV.err,1,sd)
    selected_current<-which.min(CV.err.mean)              
    selected<-c(selected,candidates[selected_current])
    print(paste("Round ",round," ; Selected feature: ",candidates[selected_current]," ; CV error=",round(CV.err.mean[selected_current],digits=4), " ; std dev=",round(CV.err.sd[selected_current],digits=4)))

}
    


