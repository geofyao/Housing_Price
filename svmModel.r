
#libraries
library(e1071)

#load the data
data<-read.csv("House Prices/datasets/train.csv")
data_test <- read.csv("House Prices/datasets/test.csv")

##get the variables that are factors to remove them from the training
factor_variables<-which(sapply(data[1,],class)=="factor")
data_preprocessed<-data[,-factor_variables]
factor_variables_test<-which(sapply(data_test[1,],class)=="factor")
data_test_preprocessed<-data_test[,-factor_variables_test]

#replace missing values
replace_na_with_mean_value<-function(vec) {
  mean_vec<-mean(vec,na.rm=T)
  vec[is.na(vec)]<-mean_vec
  vec
}
data_preprocessed<-data.frame(apply(data_preprocessed,2,replace_na_with_mean_value))
data_test_preprocessed<-data.frame(apply(data_test_preprocessed,2,replace_na_with_mean_value))

computeRMSLE <-function(Y.hat,Y){
  Y.hat.ts[Y.hat<0] <- 0    
  Y.hat.log <- log(Y.hat+1) 
  Y.log <- log(Y+1)
  avg <- mean(Y.hat.log- Y.log)^2      
  sqrt(avg)
  
}

set.seed(3)

X<-data_preprocessed[,setdiff(colnames(data_preprocessed),"SalePrice")]
Y<-data_preprocessed[,"SalePrice"]

N<-nrow(X)    #Number of examples
n<-ncol(X)    #Number of input variables

#keep the variables that gave good results after the features selection
variables_to_keep <- c(5,17,13,8,12,6,7,4,25,15,14,28,10,26)   #LM  c(5,17,6,27,18,2,25,4,33,8,29,37,20)

X_wrapped <- X[,variables_to_keep]
X.ts <- data_test_preprocessed[,colnames(X_wrapped)]

#Use severals models
size.CV<-floor(nrow(X.ts)/10)
R<-20

CV.err<-numeric(10)

Y.hat.ts.R<-matrix(0,nrow=nrow(X.ts),ncol=R)

for (r in 1:R) {
  i.tr.resample<-sample(nrow(X.ts),replace = T)   ### Complete code: Resample training set with replacement
  X.tr<-X_wrapped[i.tr.resample,]
  Y.tr<-Y[i.tr.resample]  
  
  DS<-cbind(X.tr,SalePrice=log(Y.tr))        
  model<- lm(SalePrice~.,DS)
  
  Y.hat.ts.R[,r]<- predict(model,X.ts)
  
}

Y.hat.ts<-apply(Y.hat.ts.R,1,mean)

print("ready")
Y.hat.ts.final <- cbind(data_test$Id,exp(Y.hat.ts))
colnames(Y.hat.ts.final)<- c("Id","SalePrice")
write.csv(Y.hat.ts.final, file = "House Prices/subSVMLog.csv",row.names = FALSE)
